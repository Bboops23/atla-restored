#Written by Markus Ols�n
#These are events specifically made to generate favors

#ZE.8100 - ZE.8199

namespace = ZE

#You were too drunk after a council meeting and fell on your face
character_event = {
	id = ZE.8100
	picture = GFX_evt_feast
	border = GFX_event_normal_frame_diplomacy
	desc = EVTDESC_ZE_8100

	hide_from = yes

	min_age = 16
	capable_only = yes
	prisoner = no

	is_triggered_only = yes

	trigger = {
		has_dlc = "Zeus"
		OR = {
			NOT = {	religion_group = muslim }
			AND = {
				religion_group = muslim
				trait = decadent
			}
		}
		NOR = { 
			trait = temperate
			trait = in_hiding
			trait = drunkard
		}
		any_vassal = {
			NOR = { 
				character = ROOT
				holds_favor_on = ROOT
				trait = incapable
				trait = in_hiding
				is_rival = ROOT
			}
			is_voter = yes
			prisoner = no
		}
	}

	weight_multiplier = {
		days = 1
		
		modifier = {
			factor = 2
			trait = gluttonous
		}
		modifier = {
			factor = 2
			trait = hedonist
		}
	}

	immediate = {
		random_vassal = {
			limit = {
				NOR = { 
					character = ROOT
					holds_favor_on = ROOT
					trait = incapable
					trait = in_hiding
					is_rival = ROOT
				}
				is_voter = yes
				prisoner = no
			}
			save_event_target_as = target_voter
		}
	}

	#call for help
	option = {
		name = EVTOPTA_ZE_8100
		custom_tooltip = {
			text = EVTOPTA_ZE_8100_TOOLTIP
			hidden_tooltip = {
				event_target:target_voter = {
					character_event = { id = ZE.8101 }
				}
			}
		}
	}
	#dont embarass yourself
	option = {
		name = EVTOPTB_ZE_8100
		random_list = {
			60 = {}
			30 = {
				if = { limit = { NOR = { trait = wounded trait = maimed } }
					add_trait = wounded
					hidden_tooltip = {
						character_event = {
							id = 38280 #Notify Wounded
						}
					}
				}
			}
			10 = { death = { death_reason = death_accident } }
		}
	}
}

#Councillor came to see what was wrong
character_event = {
	id = ZE.8101
	picture = GFX_evt_feast
	border = GFX_event_normal_frame_diplomacy
	desc = EVTDESC_ZE_8101

	is_triggered_only = yes

	#Helps wounded
	option = {
		name = EVTOPTA_ZE_8101
		tooltip = {
			add_favor = FROM
		}
		custom_tooltip = {
			text = EVTOPTA_ZE_8101_TOOLTIP
			FROM = {
				hidden_tooltip = {
					character_event = { id = ZE.8102 }
				}
			}
		}
	}
	#Mocks wounded
	option = {
		name = EVTOPTB_ZE_8101
		FROM = {
			tooltip = { 
				if = { limit = { NOT = { trait = drunkard } }
					add_trait = drunkard
				}
				add_rival = ROOT
			}
			hidden_tooltip = {
				character_event = { id = ZE.8103 }
			}
		}
	}
}

#councillor saved you
character_event = {
	id = ZE.8102
	picture = GFX_evt_feast
	border = GFX_event_normal_frame_diplomacy
	desc = EVTDESC_ZE_8102

	is_triggered_only = yes

	option = {
		name = EVTOPTA_ZE_8102
		reverse_add_favor = FROM
	}
}

#councillor left you
character_event = {
	id = ZE.8103
	picture = GFX_evt_feast
	border = GFX_event_normal_frame_diplomacy
	desc = EVTDESC_ZE_8103

	is_triggered_only = yes

	option = {
		name = EVTOPTA_ZE_8103
		if = { limit = { NOT = { trait = drunkard } }
			add_trait = drunkard
		}
		add_rival = FROM
	}
}

#intervene in marriage quarrel
character_event = {
	id = ZE.8110
	picture = GFX_evt_quarrel
	border = GFX_event_normal_frame_diplomacy
	desc = EVTDESC_ZE_8110

	min_age = 16
	capable_only = yes
	prisoner = no

	is_triggered_only = yes

	trigger = {
		has_dlc = "Zeus"
		NOT = { trait = in_hiding }
		any_vassal = {
			NOR = { 
				character = ROOT
				owes_favor_to = ROOT
				trait = incapable
				trait = in_hiding
			}
			is_voter = yes
			prisoner = no
			spouse = {
				NOR = {
					trait = incapable
					trait = in_hiding
					opinion = { who = PREV value = 0 }
					has_lover = yes
				}
			}
		}
	}

	immediate = {
		random_vassal = {
			limit = {
				NOR = { 
					character = ROOT
					owes_favor_to = ROOT
					trait = incapable
					trait = in_hiding
				}
				is_voter = yes
				prisoner = no
				spouse = {
					NOR = {
						trait = incapable
						trait = in_hiding
						opinion = { who = PREV value = 0 }
						has_lover = yes
					}
				}
			}
			save_event_target_as = target_vassal
		}
	}

	#interrupt their quarrel
	option = {
		name = EVTOPTA_ZE_8110
		hidden_tooltip = { 
			event_target:target_vassal = {
				character_event = { id = ZE.8112 }
			}
		}
	}
	#dont mess with others affairs
	option = {
		name = EVTOPTB_ZE_8110
	}
}

##liege helped out
#character_event = {
#	id = ZE.8111
#	picture = GFX_evt_feast
#	border = GFX_event_normal_frame_diplomacy
#	desc = EVTDESC_ZE_8111
#
#	is_triggered_only = yes
#
#	option = {
#		name = EVTOPTA_ZE_8111
#		event_target:target_vassal = {
#			hidden_tooltip = { character_event = { id = ZE.8112 } }
#		}
#	}
#}

#vassal informed of event
character_event = {
	id = ZE.8112
	picture = GFX_evt_quarrel
	border = GFX_event_normal_frame_diplomacy
	desc = EVTDESC_ZE_8112

	is_triggered_only = yes

	option = {
		name = EVTOPTA_ZE_8112
		tooltip = {
			reverse_add_favor = FROM
			FROM = {
				opinion = {
					who = ROOT
					modifier = approves_of_my_behaviour
					years = 3
				}
			}
		}
		spouse = {
			opinion = {
				who = ROOT
				modifier = opinion_happy
				years = 3
			}
		}
		FROM = { hidden_tooltip = { character_event = { id = ZE.8113 } } }
	}

	option = {
		name = EVTOPTB_ZE_8112
		tooltip = {
			FROM = {
				opinion = {
					who = ROOT
					modifier = insulted
					years = 3
				}
			}
		}
		FROM = { hidden_tooltip = { character_event = { id = ZE.8114 } } }
	}
}

#liege is appreciated
character_event = {
	id = ZE.8113
	picture = GFX_evt_quarrel
	border = GFX_event_normal_frame_diplomacy
	desc = EVTDESC_ZE_8113

	is_triggered_only = yes

	option = {
		name = EVTOPTA_ZE_8113
		add_favor = FROM
		opinion = {
			who = FROM
			modifier = approves_of_my_behaviour
			years = 3
		}
	}
}

#liege's 'help' was not appreciated
character_event = {
	id = ZE.8114
	picture = GFX_evt_poke_king_in_chest
	border = GFX_event_normal_frame_diplomacy
	desc = EVTDESC_ZE_8114

	is_triggered_only = yes

	option = {
		name = EVTOPTA_ZE_8114
		opinion = {
			who = FROM
			modifier = insulted
			years = 3
		}
	}
}