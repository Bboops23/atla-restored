# This file holds the diseases from before The Reaper's due, with all the old values
# note the "rip = no" -- this will exclude a disease from The Reaper's Due disease database, so that we can have both versions of the disease
# if "rip" is not specified, the disease will be available in both version (with the same values in both) and doesn't need to be set twice

deadly_purple_pentapox = {
	rip = no
	contagiousness = 0.125
	outbreak_chance = 0.005
	effect = {
		local_tax_modifier = -0.1
		supply_limit = -2
		max_attrition = 0.025
	}
	icon = 5
	tooltip = DEADLYPURPLEPENTAPOXINFO
	months = 18
	trait = has_pentapox
	always_get_message = no
	color = { 51 0 12 }

	timeperiod = {
		start_date = 500.1.1
		end_date = 920.1.1	

		one_only = no
	}
}

takun_cough = {
	rip = no
	contagiousness = 0.13
	outbreak_chance = 0.006
	effect = {
		local_tax_modifier = -0.40
		supply_limit = -3
		max_attrition = 0.075
	}
	icon = 1
	tooltip = TAKUNCOUGHINFO
	months = 30
	trait = has_takuan_cough
	always_get_message = no
	color = { 18 61 55 }

	timeperiod = {
		start_date = 500.1.1
		end_date = 1452.1.1	

		one_only = no
	}
}

black_frost = {
	rip = no
	contagiousness = 0.13
	outbreak_chance = 0.007
	effect = {
		local_tax_modifier = -0.40
		supply_limit = -3
		max_attrition = 0.075
	}
	icon = 3
	tooltip = BLACKFROSTINFO
	months = 18
	trait = has_black_frost
	always_get_message = no
	color = { 105 104 96 }

	timeperiod = {
		start_date = 500.1.1
		end_date = 1452.1.1	

		one_only = no
	}
}

scorch_blight = {
	rip = no
	contagiousness = 0.135
	outbreak_chance = 0.012
	effect = {
		local_tax_modifier = -0.25
		supply_limit = -3
		max_attrition = 0.05
	}
	icon = 6
	tooltip = SCORCHBLIGHTINFO
	months = 18
	trait = has_scorch_blight
	always_get_message = no
	color = { 222 194 14 }

	timeperiod = {
		start_date = 500.1.1
		end_date = 1452.1.1	

		one_only = no
	}
}

verminbite = {
	rip = no
	contagiousness = 0.12
	outbreak_chance = 0.005
	effect = {
		local_tax_modifier = -0.25
		supply_limit = -2
		max_attrition = 0.05
	}
	icon = 1
	tooltip = VERMINBITEINFO
	months = 30
	trait = has_verminbite
	always_get_message = no
	color = { 207 238 177 }

	timeperiod = {
		start_date = 500.1.1
		end_date = 1452.1.1	

		one_only = no
	}
}

ashbreath = {
	rip = no
	contagiousness = 0.75
	outbreak_chance = 0.01
	effect = {
		local_tax_modifier = -0.95
		supply_limit = -1
	}
	icon = 4
	tooltip = ASHBREATHINFO
	months = 6
	trait = has_ashbreath
	always_get_message = yes
	color = { 255 0 0 }

	timeperiod = {
		start_date = 831.1.1
		end_date = 863.1.1	

		one_only = yes
	}
}