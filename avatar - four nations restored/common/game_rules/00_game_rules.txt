#
# List of options showing in the Game Rules screen
#
# format is:
# rule_token = {
#	name = "TEXT_KEY_FOR_NAME"
#	desc = "TEXT_KEY_FOR_LONG_DESC"
#	option = {										# please note that the first option is always the default option
#		name = option_token
#		text = "TEXT_KEY_FOR_OPTION_NAME"
#		achievements = no							# by default achievements = yes, allows or disallows achievements
#	}
# }

campaign_events = {
	name = "RULE_CAMPAIGN_EVENTS"
	option = {
		name = on
		text = "RULE_CAMPAIGN_EVENTS_OPTION_ON"
		desc = "RULE_CAMPAIGN_EVENTS_ON_DESC"
	}
	option = {
		name = off
		text = "RULE_CAMPAIGN_EVENTS_OPTION_OFF"
		desc = "RULE_CAMPAIGN_EVENTS_OFF_DESC"
		achievements = no
	}
}

bending_events = {
	name = "RULE_BENDING_EVENTS"
	option = {
		name = canon
		text = "RULE_BENDING_EVENTS_OPTION_CANON"
		desc = "RULE_BENDING_EVENTS_CANON_DESC"
	}
	option = {
		name = increased
		text = "RULE_BENDING_EVENTS_OPTION_INCREASED"
		desc = "RULE_BENDING_EVENTS_INCREASED_DESC"
		achievements = no
	}
	option = {
		name = rare
		text = "RULE_BENDING_EVENTS_OPTION_RARE"
		desc = "RULE_BENDING_EVENTS_RARE_DESC"
		achievements = no
	}
}

# earth_kingdom_status = {
	# name = "RULE_EK_STATUS"
	# option = {
		# name = canon
		# text = "RULE_EK_STATUS_CANON"
		# desc = "RULE_EK_STATUS_CANON_DESC"
	# }
	# option = {
		# name = united
		# text = "RULE_EK_STATUS_UNITED"
		# desc = "RULE_EK_STATUS_UNITED_DESC"
		# achievements = no
	# }
	# option = {
		# name = dismantled
		# text = "RULE_EK_STATUS_DISMANTLED"
		# desc = "RULE_EK_STATUS_DISMANTLED_DESC"
		# achievements = no
	# }
# }

canon_events = {
	name = "RULE_CANON_EVENTS"
	option = {
		name = four_nations
		text = "RULE_CANON_EVENTS_FREE"
		desc = "RULE_CANON_EVENTS_FREE_DESC"
	}
	option = {
		name = semi_canon
		text = "RULE_CANON_EVENTS_SEMI_CANON"
		desc = "RULE_CANON_EVENTS_SEMI_CANON_DESC"
		achievements = no
	}
	option = {
		name = strict_canon
		text = "RULE_CANON_EVENTS_CANON"
		desc = "RULE_CANON_EVENTS_CANON_DESC"
		achievements = no
	}
}

unjustified_wars = {
	name = "RULE_UNJUSTIFIED_WARS"
	dlc = "Jade Dragon"
	group = "RULE_GROUP_WARFARE"
	group = "RULE_GROUP_JD"
	option = {
		name = on
		text = "RULE_OPTION_ON"
		desc = "RULE_UNJUSTIFIED_ON_DESC"
	}
	option = {
		name = off
		text = "RULE_OPTION_OFF"
		desc = "RULE_UNJUSTIFIED_OFF_DESC"
	}
}

jade_dragon_cbs = {
	name = "RULE_JD_CBS"
	dlc = "Jade Dragon"
	group = "RULE_GROUP_WARFARE"
	group = "RULE_GROUP_JD"
	option = {
		name = on
		text = "RULE_OPTION_ON"
		desc = "RULE_JD_CBS_ON_DESC"
	}
	option = {
		name = off
		text = "RULE_OPTION_OFF"
		desc = "RULE_JD_CBS_OFF_DESC"
	}
}

epidemics = {
	name = "RULE_EPIDEMICS"
	dlc = "The Reaper's Due"
	option = {
		name = dynamic
		text = "RULE_EPIDEMICS_DYNAMIC"
		desc = "RULE_EPIDEMICS_DYNAMIC_DESC"
	}
	option = {
		name = historical
		text = "RULE_OPTION_HISTORICAL"
		desc = "RULE_EPIDEMICS_HISTORICAL_DESC"
	}
	option = {
		name = deadly
		text = "RULE_EPIDEMICS_DEADLY"
		desc = "RULE_EPIDEMICS_DEADLY_DESC"
	}
}

minor_epidemics = {
	name = "RULE_MINOR_EPIDEMICS"
	dlc = "The Reaper's Due"
	option = {
		name = default
		text = "RULE_OPTION_DEFAULT"
		desc = "RULE_EPIDEMICS_DEFAULT_DESC"
	}
	option = {
		name = more
		text = "RULE_OPTION_MORE"
		desc = "RULE_EPIDEMICS_MORE_DESC"
	}
	option = {
		name = fewer
		text = "RULE_FEWER"
		desc = "RULE_EPIDEMICS_FEWER_DESC"
	}
}

secret_cults = {
	name = "RULE_SECRET_CULTS"
	group = "RULE_GROUP_VARIOUS"
	option = {
		name = default
		text = "RULE_OPTION_DEFAULT"
		desc = "RULE_SECRET_CULTS_DEFAULT_DESC"
	}
	option = {
		name = none
		text = "RULE_OPTION_NONE"
		desc = "RULE_SECRET_CULTS_NONE_DESC"
	}
}

shattered_retreat = {
	name = "RULE_SHATTERED_RETREAT"
	option = {
		name = on
		text = "RULE_OPTION_ON"
		desc = "RULE_SHATTERED_RETREAT_ON_DESC"
	}
	option = {
		name = off
		text = "RULE_OPTION_OFF"
		desc = "RULE_SHATTERED_RETREAT_OFF_DESC"
	}
}

siege_assaults = {
	name = "RULE_SIEGE_ASSAULTS"
	group = "RULE_GROUP_WARFARE"
	option = {
		name = on
		text = "RULE_OPTION_ON"
		desc = "RULE_SIEGE_ASSAULTS_ON_DESC"
	}
	option = {
		name = unlimited
		text = "RULE_OPTION_UNLIMITED"
		desc = "RULE_SIEGE_ASSAULTS_UNLIMITED_DESC"
	}
	option = {
		name = off
		text = "RULE_OPTION_OFF"
		desc = "RULE_SIEGE_ASSAULTS_OFF_DESC"
	}
}

siege_events = {
	name = "RULE_SIEGE_EVENTS"
	group = "RULE_GROUP_WARFARE"
	option = {
		name = on
		text = "RULE_OPTION_ON"
		desc = "RULE_SIEGE_EVENTS_ON_DESC"
	}
	option = {
		name = defenders
		text = "RULE_OPTION_DEFENDERS"
		desc = "RULE_SIEGE_EVENTS_DEFENDERS_DESC"
	}
	option = {
		name = off
		text = "RULE_OPTION_OFF"
		desc = "RULE_SIEGE_EVENTS_OFF_DESC"
	}
}

defensive_pacts = {
	name = "RULE_DEFENSIVE_PACTS"
	option = {
		name = off
		text = "RULE_OPTION_OFF"
		desc = "RULE_DEFENSIVE_PACTS_OFF_DESC"
	}
	option = {
		name = on
		text = "RULE_OPTION_ON"
		desc = "RULE_DEFENSIVE_PACTS_ON_DESC"
	}
}

gender = {
	name = "RULE_GENDER"
	option = {
		name = default
		text = "RULE_OPTION_DEFAULT"
		desc = "RULE_GENDER_DEFAULT_DESC"
	}
	option = {
		name = historical
		text = "RULE_OPTION_HISTORICAL"
		desc = "RULE_GENDER_HISTORICAL_DESC"
	}
	option = {
		name = all
		achievements = no
		text = "RULE_OPTION_ALL"
		desc = "RULE_GENDER_ALL_DESC"
	}
}

dejure_drift = {
	name = "RULE_DEJURE"
	option = {
		name = default
		text = "RULE_OPTION_DEFAULT"
		desc = "RULE_DEJURE_DEFAULT_DESC"
	}
	option = {
		name = restricted
		text = "RULE_OPTION_RESTRICTED"
		desc = "RULE_DEJURE_RESTRICTED_DESC"
	}
	option = {
		name = off
		text = "RULE_OPTION_OFF"
		desc = "RULE_DEJURE_OFF_DESC"
	}
}

dejure_drift_duration = {
	name = "RULE_DEJURE_DURATION"
	option = {
		name = default
		text = "RULE_OPTION_DEFAULT"
		desc = "RULE_DEJURE_DURATION_DEFAULT_DESC"
	}
	option = {
		name = longer
		text = "RULE_OPTION_LONGER"
		desc = "RULE_DEJURE_DURATION_LONGER_DESC"
	}
	option = {
		name = shortest
		achievements = no
		text = "RULE_OPTION_SHORTEST"
		desc = "RULE_DEJURE_DURATION_SHORTEST_DESC"
	}
	option = {
		name = shorter
		achievements = no
		text = "RULE_OPTION_SHORTER"
		desc = "RULE_DEJURE_DURATION_SHORTER_DESC"
	}
}

raiding = {
	name = "RULE_RAIDING"
	option = {
		name = historical
		text = "RULE_OPTION_HISTORICAL"
		desc = "RULE_RAIDING_HISTORICAL_DESC"
	}
	option = {
		name = unrestricted
		achievements = no
		text = "RULE_OPTION_UNRESTRICTED"
		desc = "RULE_RAIDING_UNRESTRICTED_DESC"
	}
	option = {
		name = off
		achievements = no
		text = "RULE_OPTION_OFF"
		desc = "RULE_RAIDING_OFF_DESC"
	}
}

adventurers = {
	name = "RULE_ADVENTURERS"
	option = {
		name = normal
		text = "RULE_OPTION_NORMAL"
		desc = "RULE_ADVENTURERS_NORMAL_DESC"
	}
	option = {
		name = rare
		achievements = no
		text = "RULE_OPTION_RARE"
		desc = "RULE_ADVENTURERS_RARE_DESC"
	}
	option = {
		name = none
		achievements = no
		text = "RULE_OPTION_NONE"
		desc = "RULE_ADVENTURERS_NONE_DESC"
	}
}

interfaith_marriages = {
	name = "RULE_INTERFAITH_MARRIAGES"
	option = {
		name = restricted
		text = "RULE_OPTION_RESTRICTED"
		desc = "RULE_IFM_RESTRICTED_DESC"
	}
	option = {
		name = open
		text = "RULE_OPTION_OPEN"
		desc = "RULE_IFM_OPEN_DESC"
	}
}

matrilineal_marriages = {
	name = "RULE_MATRILINEAL_MARRIAGES"
	option = {
		name = on
		text = "RULE_OPTION_ON"
		desc = "RULE_MATRILINEAL_ON_DESC"
	}
	option = {
		name = off
		text = "RULE_OPTION_OFF"
		desc = "RULE_MATRILINEAL_OFF_DESC"
	}
}

dynamic_realms = {
	name = "RULE_DYNAMIC_REALMS"
	dlc = "Charlemagne"
	option = {
		name = on
		text = "RULE_OPTION_ON"
		desc = "RULE_DYNAMIC_REALMS_ON_DESC"
	}
	option = {
		name = off
		text = "RULE_OPTION_OFF"
		desc = "RULE_DYNAMIC_REALMS_OFF_DESC"
	}
}

vassal_republics = {
	name = "RULE_VASSAL_REPUBLICS"
	option = {
		name = restricted
		text = "RULE_OPTION_RESTRICTED"
		desc = "RULE_VASSAL_REPUBLICS_RESTRICTED_DESC"
	}
	option = {
		name = unrestricted
		achievements = no
		text = "RULE_OPTION_UNRESTRICTED"
		desc = "RULE_VASSAL_REPUBLICS_UNRESTRICTED_DESC"
	}
}

vassal_theocracies = {
	name = "RULE_VASSAL_THEOCRACIES"
	option = {
		name = restricted
		text = "RULE_OPTION_RESTRICTED"
		desc = "RULE_VASSAL_THEOCRACIES_RESTRICTED_DESC"
	}
	option = {
		name = unrestricted
		achievements = no
		text = "RULE_OPTION_UNRESTRICTED"
		desc = "RULE_VASSAL_THEOCRACIES_UNRESTRICTED_DESC"
	}
}

invitation = {
	name = "RULE_INVITATION"
	option = {
		name = default
		text = "RULE_OPTION_DEFAULT"
		desc = "RULE_INVITATION_DEFAULT_DESC"
	}
	option = {
		name = open
		achievements = no
		text = "RULE_OPTION_OPEN"
		desc = "RULE_INVITATION_OPEN_DESC"
	}
}

diplo_range = {
	name = "RULE_DIPLO_RANGE"
	option = {
		name = on
		text = "RULE_OPTION_ON"
		desc = "RULE_DIPLO_RANGE_ON_DESC"
	}
	option = {
		name = restricted
		text = "RULE_OPTION_RESTRICTED"
		desc = "RULE_DIPLO_RANGE_RESTRICTED_DESC"
	}
	option = {
		name = off
		text = "RULE_OPTION_OFF"
		desc = "RULE_DIPLO_RANGE_OFF_DESC"
	}
}

regencies = {
	name = "RULE_REGENCIES"
	option = {
		name = on
		text = "RULE_OPTION_ON"
		desc = "RULE_REGENCIES_ON_DESC"
	}
	option = {
		name = off
		achievements = no
		text = "RULE_OPTION_OFF"
		desc = "RULE_REGENCIES_OFF_DESC"
	}
}

provincial_revolts = {
	name = "RULE_PROVINCE_REVOLTS"
	option = {
		name = normal
		text = "RULE_OPTION_NORMAL"
		desc = "RULE_PROVINCE_REVOLTS_NORMAL_DESC"
	}
	option = {
		name = rare
		achievements = no
		text = "RULE_OPTION_RARE"
		desc = "RULE_PROVINCE_REVOLTS_RARE_DESC"
	}
	option = {
		name = none
		achievements = no
		text = "RULE_OPTION_NONE"
		desc = "RULE_PROVINCE_REVOLTS_NONE_DESC"
	}
}

provincial_revolt_strength = {
	name = "RULE_PROVINCIAL_REVOLT_STRENGTH"
	group = "RULE_GROUP_WARFARE"
	dlc = "The Old Gods"
	option = {
		name = default
		text = "RULE_OPTION_DEFAULT"
		desc = "RULE_PROVINCIAL_REVOLT_STRENGTH_DEFAULT_DESC"
	}
	option = {
		name = powerful
		text = "RULE_OPTION_POWERFUL"
		desc = "RULE_PROVINCIAL_REVOLT_STRENGTH_POWERFUL_DESC"
	}
	option = {
		name = very_powerful
		text = "RULE_OPTION_VERY_POWERFUL"
		desc = "RULE_PROVINCIAL_REVOLT_STRENGTH_VERY_POWERFUL_DESC"
	}
	option = {
		name = extremely_powerful
		text = "RULE_OPTION_EXTREMELY_POWERFUL"
		desc = "RULE_PROVINCIAL_REVOLT_STRENGTH_EXTREMELY_POWERFUL_DESC"
	}
}

defensive_attrition = {
	name = "RULE_DEF_ATTRITION"
	option = {
		name = on
		text = "RULE_OPTION_ON"
		desc = "RULE_DEF_ATTRITION_ON_DESC"
	}
	option = {
		name = tribal
		achievements = no
		text = "RULE_DEF_ATTRITION_TRIBAL"
		desc = "RULE_DEF_ATTRITION_TRIBAL_DESC"
	}
	option = {
		name = off
		achievements = no
		text = "RULE_OPTION_OFF"
		desc = "RULE_DEF_ATTRITION_OFF_DESC"
	}
}

diplo_assassination = {
	name = "RULE_DIPLO_ASSASSINATION"
	option = {
		name = off
		text = "RULE_DIPLO_ASSASSINATION_OFF"
		desc = "RULE_DIPLO_ASSASSINATION_OFF_DESC"
	}
	option = {
		name = on
		achievements = no
		text = "RULE_DIPLO_ASSASSINATION_ON"
		desc = "RULE_DIPLO_ASSASSINATION_ON_DESC"
	}
}

ai_seduction = {
	name = "RULE_AI_SEDUCTION"
	dlc = "Way of Life"
	option = {
		name = on
		text = "RULE_OPTION_ON"
		desc = "RULE_AI_SEDUCTION_ON_DESC"
	}
	option = {
		name = off
		text = "RULE_OPTION_OFF"
		desc = "RULE_AI_SEDUCTION_OFF_DESC"
	}
}

ai_intrigue = {
	name = "RULE_AI_INTRIGUE"
	dlc = "Way of Life"
	option = {
		name = on
		text = "RULE_OPTION_ON"
		desc = "RULE_AI_INTRIGUE_ON_DESC"
	}
	option = {
		name = off
		text = "RULE_OPTION_OFF"
		desc = "RULE_AI_INTRIGUE_OFF_DESC"
	}
}

dueling = {
	name = "RULE_DUELING"
	dlc = "Way of Life"
	group = "RULE_GROUP_DIPLOMACY"
	group = "RULE_GROUP_WOL"
	option = {
		name = default
		text = "RULE_OPTION_DEFAULT"
		desc = "RULE_DUELING_DEFAULT_DESC"
	}
	option = {
		name = restricted
		text = "RULE_OPTION_RESTRICTED"
		desc = "RULE_DUELING_RESTRICTED_DESC"
		achievements = no
	}
	option = {
		name = unrestricted
		text = "RULE_OPTION_UNRESTRICTED"
		desc = "RULE_DUELING_UNRESTRICTED_DESC"
		achievements = no
	}
}

punishment_release_prisoner = {
	name = "RULE_PUNISHMENT_RELEASE_PRISONER"
	group = "RULE_GROUP_DIPLOMACY"
	option = {
		name = on
		text = "RULE_OPTION_ON"
		desc = "RULE_PUNISHMENT_RELEASE_PRISONER_ON_DESC"
	}
	option = {
		name = off
		text = "RULE_OPTION_OFF"
		desc = "RULE_PUNISHMENT_RELEASE_PRISONER_OFF_DESC"
		achievements = no
	}
}

dynasty_title_names = {
	name = "RULE_DYNASTY_TITLE_NAMES"
	group = "RULE_GROUP_VARIOUS"
	option = {
		name = on
		text = "RULE_OPTION_ON"
		desc = "RULE_DYNASTY_TITLE_NAMES_ON_DESC"
	}
	option = {
		name = off
		text = "RULE_OPTION_OFF"
		desc = "RULE_DYNASTY_TITLE_NAMES_OFF_DESC"
	}
}

cultural_title_names = {
	name = "RULE_CULTURAL_TITLE_NAMES"
	group = "RULE_GROUP_VARIOUS"
	option = {
		name = on
		text = "RULE_OPTION_ON"
		desc = "RULE_CULTURAL_TITLE_NAMES_ON_DESC"
	}
	option = {
		name = off
		text = "RULE_OPTION_OFF"
		desc = "RULE_CULTURAL_TITLE_NAMES_OFF_DESC"
	}
}


demesne_size = {
	name = "RULE_DEMESNE_SIZE"
	group = "RULE_GROUP_DIPLOMACY"
	option = {
		name = default
		text = "RULE_OPTION_DEFAULT"
		desc = "RULE_DEMESNE_SIZE_DEFAULT_DESC"
	}
	option = {
		name = unlimited
		text = "RULE_OPTION_UNLIMITED"
		desc = "RULE_DEMESNE_SIZE_UNLIMITED_DESC"
		achievements = no
	}
	option = {
		name = quartered
		text = "RULE_OPTION_QUARTERED"
		desc = "RULE_DEMESNE_SIZE_QUARTERED_DESC"
	}
	option = {
		name = half
		text = "RULE_OPTION_HALF"
		desc = "RULE_DEMESNE_SIZE_HALF_DESC"
	}
}

vassal_limit = {
	name = "RULE_VASSAL_LIMIT"
	group = "RULE_GROUP_DIPLOMACY"
	option = {
		name = default
		text = "RULE_OPTION_DEFAULT"
		desc = "RULE_VASSAL_LIMIT_DEFAULT_DESC"
	}
	option = {
		name = unlimited
		text = "RULE_OPTION_UNLIMITED"
		desc = "RULE_VASSAL_LIMIT_UNLIMITED_DESC"
		achievements = no
	}
	option = {
		name = quartered
		text = "RULE_OPTION_QUARTERED"
		desc = "RULE_VASSAL_LIMIT_QUARTERED_DESC"
	}
	option = {
		name = half
		text = "RULE_OPTION_HALF"
		desc = "RULE_VASSAL_LIMIT_HALF_DESC"
	}
}

grant_independence = {
	name = "RULE_GRANT_INDEPENDENCE"
	group = "RULE_GROUP_DIPLOMACY"
	option = {
		name = restricted
		text = "RULE_OPTION_RESTRICTED"
		desc = "RULE_GRANT_INDEPENDENCE_RESTRICTED_DESC"
	}
	option = {
		name = unrestricted
		text = "RULE_OPTION_UNRESTRICTED"
		desc = "RULE_GRANT_INDEPENDENCE_UNRESTRICTED_DESC"
		achievements = no
	}
}

conclave_education_focuses = {
	name = "RULE_EDU_FOCUSES"
	dlc = "Conclave"
	group = "RULE_GROUP_CONCLAVE"
	option = {
		name = close_relatives
		text = "RULE_EDU_FOCUSES_CR"
		desc = "RULE_EDU_FOCUSES_CR_DESC"
	}
	option = {
		name = all
		text = "RULE_OPTION_ALL"
		desc = "RULE_EDU_FOCUSES_ALL_DESC"
	}
	option = {
		name = minimal
		text = "RULE_OPTION_MINIMAL"
		desc = "RULE_EDU_FOCUSES_MINIMAL_DESC"
	}
}
